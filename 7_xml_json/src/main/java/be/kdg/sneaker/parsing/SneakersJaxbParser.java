package be.kdg.sneaker.parsing;

import be.kdg.sneaker.data.Data;
import be.kdg.sneaker.model.Sneakers;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class SneakersJaxbParser {

    public static void JaxbWriteXml(String file, Object root) {
        try {
            JAXBContext context = JAXBContext.newInstance(Sneakers.class);
            Marshaller marshaller = context.createMarshaller();

            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            marshaller.marshal(root, new File(file));
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }


    public static <T> T JaxbReadXml(String file, Class<T> typeParameterClass) {
        try {
            JAXBContext context = JAXBContext.newInstance(typeParameterClass);
            Unmarshaller unmarshaller = context.createUnmarshaller();

            return (T) unmarshaller.unmarshal(new File(file));
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return null;
    }

}
