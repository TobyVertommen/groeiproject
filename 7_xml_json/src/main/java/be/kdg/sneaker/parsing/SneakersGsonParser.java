package be.kdg.sneaker.parsing;

import be.kdg.sneaker.model.Sneaker;
import be.kdg.sneaker.model.Sneakers;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.*;
import java.util.Arrays;
import java.util.List;

public class SneakersGsonParser {

    public static void writeJson(Sneakers sneakers, String fileName){
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.setPrettyPrinting().create();
        String jsonString = gson.toJson(sneakers);

        try (FileWriter writer = new FileWriter(fileName)) {
            writer.write(jsonString);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static Sneakers readJson(String fileName){
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.setPrettyPrinting().create();

        try( BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
           return gson.fromJson(reader, Sneakers.class);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


}
