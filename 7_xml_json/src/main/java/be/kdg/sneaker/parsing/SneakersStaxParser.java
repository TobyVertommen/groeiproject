package be.kdg.sneaker.parsing;

import be.kdg.sneaker.model.Sneaker;
import be.kdg.sneaker.model.Sneakers;
import com.sun.xml.txw2.output.IndentingXMLStreamWriter;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class SneakersStaxParser {
    private Sneakers sneakers;
    private XMLStreamWriter writer;

    public SneakersStaxParser(Sneakers sneakers, String path) {
        this.sneakers = sneakers;
        XMLOutputFactory factory = XMLOutputFactory.newInstance();
        try {
            writer = factory.createXMLStreamWriter(new FileWriter(path));
            writer = new IndentingXMLStreamWriter(writer);

            staxWriteXML();
            writer.close();

        } catch (IOException | XMLStreamException e) {
            e.printStackTrace();
        }

    }

    void staxWriteXML(){
        try {
            writer.writeStartDocument();
            writer.writeStartElement("sneakers");

            sneakers.sneakerSet.forEach(this::writeElement);

            writer.writeEndElement();
            writer.writeEndDocument();

        } catch (XMLStreamException e) {
            e.printStackTrace();
        }

    }

    private void writeElement(Sneaker sneaker){
        try {
            writer.writeStartElement("sneaker");

            writer.writeAttribute("name", sneaker.getName());

            writer.writeStartElement("price");
            writer.writeCharacters(String.valueOf(sneaker.getPrice()));
            writer.writeEndElement();

            writer.writeStartElement("size");
            writer.writeCharacters(String.valueOf(sneaker.getSize()));
            writer.writeEndElement();

            writer.writeStartElement("profitable");
            writer.writeCharacters(String.valueOf(sneaker.isProfitable()));
            writer.writeEndElement();

            writer.writeStartElement("model");
            writer.writeCharacters(String.valueOf(sneaker.getModel()));
            writer.writeEndElement();

            writer.writeStartElement("release");
            writer.writeCharacters(String.valueOf(sneaker.getReleaseDate()));
            writer.writeEndElement();

            writer.writeEndElement();

        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
    }
}
