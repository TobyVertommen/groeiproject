package be.kdg.sneaker.parsing;

import be.kdg.sneaker.model.Model;
import be.kdg.sneaker.model.Sneaker;
import be.kdg.sneaker.model.Sneakers;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;

public class SneakersDomParser {

    public static Sneakers domReadXML(String filename) {
        Sneakers sneakers = new Sneakers();
        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = builder.parse(new File(filename));
            Element rootElement = doc.getDocumentElement();
            NodeList sneakerNodes = rootElement.getChildNodes();

            for (int i = 0; i < sneakerNodes.getLength(); i++) {
                if (sneakerNodes.item(i).getNodeType() != Node.ELEMENT_NODE) {
                    continue;
                }
                Element e = (Element) sneakerNodes.item(i);

                String name = e.getAttribute("name");

                Element price =
                        (Element) e.getElementsByTagName("price").item(0);
                double priceSn = Double.parseDouble(price.getTextContent());

                Element size = (Element) e.getElementsByTagName("size").item(0);
                int sizeSn = Integer.parseInt(size.getTextContent());

                Element profitable = (Element) e.getElementsByTagName("profitable").item(0);
                boolean profitableSn = Boolean.parseBoolean(profitable.getTextContent());

                Element model = (Element) e.getElementsByTagName("model").item(0);
                Model modelSn = Model.valueOf(model.getTextContent());

                Element release = (Element) e.getElementsByTagName("release").item(0);
                LocalDate releaseSn = LocalDate.parse(release.getTextContent());

                Sneaker sn = new Sneaker(
                        name,
                        priceSn,
                        sizeSn,
                        profitableSn,
                        modelSn,
                        releaseSn
                );
                sneakers.add(sn);

            }
        } catch (IOException | ParserConfigurationException | SAXException e) {
            e.printStackTrace();
        }
        return sneakers;
    }
}
