package be.kdg.sneaker.model;

import com.google.gson.annotations.SerializedName;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.*;
@XmlAccessorType(XmlAccessType.FIELD)
//zorgt ervoor dat er geen error meer komt en XmlElement tag staat bij property ipv setter

@XmlRootElement(name = "sneakers")
public class Sneakers {
    @XmlElement(name = "sneaker-set")
    @SerializedName("sneaker-set")
    public ArrayList<Sneaker> sneakerSet;


    public Sneakers(List<Sneaker> list) {
        this.sneakerSet = new ArrayList<Sneaker>(list);
    }

    public Sneakers() {
        this.sneakerSet = new ArrayList<Sneaker>();
    }

    public boolean add(Sneaker sneaker) {
        return this.sneakerSet.add(sneaker);
    }

    public boolean remove(String name) {
        for (Sneaker temp : sneakerSet) {
            if (temp.getName().equals(name)) {
                sneakerSet.remove(temp);
                return true;
            }
        }
        return false;
    }

    public Sneaker search(String name) {
        Sneaker s = null;
        for (Sneaker sneaker : sneakerSet) {
            if (sneaker.getName().equals(name)) {
                s = sneaker;
            }
        }
        return s;
    }

    public List<Sneaker> sortedOnName() {
        List<Sneaker> sortedOnNameList = new ArrayList<>(sneakerSet);
        Collections.sort(sortedOnNameList);
        return sortedOnNameList;
    }

    public List<Sneaker> sortedOnPrice() {
        class ByPrice implements Comparator<Sneaker> {
            @Override
            public int compare(Sneaker o1, Sneaker o2) {
                return Double.compare(o1.getPrice(), o2.getPrice());
            }
        }
        List<Sneaker> sortedOnPriceList = new ArrayList<>(sneakerSet);
        sortedOnPriceList.sort(new ByPrice());
        return sortedOnPriceList;
    }

    public List<Sneaker> sortedOnSize() {
        class BySize implements Comparator<Sneaker> {
            @Override
            public int compare(Sneaker o1, Sneaker o2) {
                return o1.getSize() - o2.getSize();
            }
        }
        List<Sneaker> sortedOnSizeList = new ArrayList<>(sneakerSet);
        sortedOnSizeList.sort(new BySize());
        return sortedOnSizeList;
    }


    public int getSize() {
        int count = 0;
        for (Sneaker sneaker : sneakerSet) {
            count++;
        }
        return count;
    }


    public ArrayList<Sneaker> getSneakerSet() {
        return sneakerSet;
    }

//    @XmlElement(name = "sneaker-set") ??????
    public void setSneakerSet(ArrayList<Sneaker> sneakerSet) {
        this.sneakerSet = sneakerSet;
    }
}
