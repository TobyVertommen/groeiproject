package be.kdg.sneaker.parsing;

import be.kdg.sneaker.data.Data;
import be.kdg.sneaker.model.Sneakers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ParserTest {
    private Sneakers sneakers;

    @BeforeEach
    void setUp() {
        sneakers = new Sneakers();
        Data.getData().forEach(sneakers::add);
    }

    @Test
    void testStaxDom() {
   //     assertDoesNotThrow(() -> new SneakersStaxParser(sneakers, "datafiles/staxSneakers.xml"));
     SneakersStaxParser stax = new SneakersStaxParser(sneakers, "datafiles/staxSneakers.xml");
     Sneakers sneakersDom = SneakersDomParser.domReadXML("datafiles/staxSneakers.xml");
     assertEquals(sneakers.sneakerSet, sneakersDom.sneakerSet);
    }

    @Test
    void testJaxb() {
        SneakersJaxbParser.JaxbWriteXml("datafiles/jaxbSneakers.xml", sneakers);
        Sneakers sneakersJaxb = SneakersJaxbParser.JaxbReadXml("datafiles/jaxbSneakers.xml", Sneakers.class);
        assertEquals(sneakers.sneakerSet, sneakersJaxb.sneakerSet);
    }

    @Test
    void testGson() {
        SneakersGsonParser.writeJson(sneakers, "datafiles/gsonSneakers.json");
        Sneakers sneakersGson = SneakersGsonParser.readJson("datafiles/gsonSneakers.json");
        assertEquals(sneakers.sneakerSet, sneakersGson.sneakerSet);
    }
}
