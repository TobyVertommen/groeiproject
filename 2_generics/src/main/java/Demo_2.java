import be.kdg.sneaker.data.Data;
import be.kdg.sneaker.generics.PriorityQueue;
import be.kdg.sneaker.model.Sneaker;

import java.util.Random;

public class Demo_2 {
    public static void main(String[] args) {

        PriorityQueue<String> myQueue = new PriorityQueue<>();
        myQueue.enqueue("Tokio", 2);
        myQueue.enqueue("Denver", 5);
        myQueue.enqueue("Rio", 2);
        myQueue.enqueue("Oslo", 3);
        System.out.println("Overzicht van de PriorityQueue:");
        System.out.println(myQueue.toString());
       System.out.println("aantal: " + myQueue.getSize());
        System.out.println("positie van Tokio: " + myQueue.search("Tokio"));
        System.out.println("positie van Nairobi: " + myQueue.search("Nairobi"));
        for(int i = 0; i < 4; i++) {
            System.out.println("Dequeue: " + myQueue.dequeue());
        }
      System.out.println("Size na dequeue: " + myQueue.getSize());



        PriorityQueue<Sneaker> myQueue2 = new PriorityQueue<>();
        for(Sneaker sneaker : Data.getData()) {
            Random random = new Random();
            myQueue2.enqueue(sneaker, random.nextInt(6));
        }

        System.out.println("Overzicht van de PriorityQueue:");
        System.out.println(myQueue2.toString());
        System.out.println("aantal: " + myQueue2.getSize());
        for (int i = 0; i < 4; i++) {
            System.out.println("Dequeue: " + myQueue2.dequeue());
        }

        System.out.println("Size na dequeue: " + myQueue2.getSize());


        PriorityQueue<Sneaker> myQueue3 = new PriorityQueue<>();
        for (var sneaker : Data.getData()) {
            var random = new Random();
            myQueue3.enqueue(sneaker, random.nextInt(6));
        }

        System.out.println("Overzicht van de PriorityQueue:");
        System.out.println(myQueue3.toString());
        System.out.println("aantal: " + myQueue3.getSize());
        for (int i = 0; i < 4; i++) {
            System.out.println("Dequeue: " + myQueue3.dequeue());
        }

        System.out.println("Size na dequeue: " + myQueue3.getSize());
    }
}
