package be.kdg.sneaker.model;

import java.time.LocalDate;
import java.util.Objects;

/**
 * Klasse Sneaker.
 * Zorgt ervoor dat de basisinformatie over een Sneaeker kan bijgehouden en
 * gemodeleerd kan worden.
 *
 * @author Toby Vertommen
 * @version 1.0
 * @see <a href="https://en.wikipedia.org/wiki/Sneakers">Sneakers</a>
 */
public class Sneaker implements Comparable<Sneaker>{
    /**
     * De naam van de Sneaker
     */
    private String name;
    private double price;
    private int size;
    /**
     * Of de sneaker al dan niet winstgevend kan zijn
     */
    private boolean profitable;
    private Model model;
    /**
     * De uitgavedatum van de sneaker
     */
    private LocalDate releaseDate;

    /**
     *  Constructor voor een Sneaker-object a.d.h.v. een naam, een prijs, een maat, winstmakend,
     *  een model en een uitgave datum.
     *
     * @param name Naam van het element
     * @param price De prijs van het element
     * @param size De juiste schoenmaat van het element
     * @param profitable Of het element al dan niet winstgevend kan zijn
     * @param model De vorm van het element [low, mid of hoog]
     * @param releaseDate De datum waarop het element uitgegeven is
     */
    public Sneaker(String name, double price, int size,boolean profitable, Model model, LocalDate releaseDate) {
        setName(name);
        setPrice(price);
        setSize(size);
        setProfitable(profitable);
        setModel(model);
        setReleaseDate(releaseDate);
    }


    /**
     * Default constructor voor een Sneaker-object
     *
     */
    public Sneaker() {
     new Sneaker("Unknown", 1, 35,false, Model.LOW, LocalDate.of(1980,1,1));
    }


    /**
     * Giet alle gegevens van een Sneaker in 1-één lijn.
     *
     * @return String van geformateerde Sneaker-info
     */
    public String toString() {
        return String.format("%-30s %15.2feur %10dEU %15s   profit = %-5b %15s", this.getName(),
                this.getPrice(),
                this.getSize(),
                this.isProfitable(),
                this.getModel(),
                this.getReleaseDate().toString());
    }


    /**
     * Geeft naam van sneaker terug
     *
     * @return De naam
     */
    public String getName() {
        return name;
    }

    /**
     * Kijkt na of de naam niet null is en voeht deze toe aan de sneaker
     *
     * @param name De naam van de sneaker
     */
    public void setName(String name) {
        if (name == null) throw new IllegalArgumentException("Name cannot be empty!");
        else this.name = name;
    }

    /**
     * Geeft prijs van sneaker terug
     *
     * @return De prijs
     */
    public double getPrice() {
        return price;
    }


    /**
     * Kijkt na of de prijs meer dan 0euro bedraagt en voegt deze toe aan de sneaker
     *
     * @param price De prijs van de sneaker
     */
    public void setPrice(double price) {
        if (price <= 0) throw new IllegalArgumentException("Price cannot be below 0 or equal to 0!");
        else this.price = price;
    }

    /**
     * Geeft maat van sneaker terug
     *
     * @return De schoenmaat
     */
    public int getSize() {
        return size;
    }

    /**
     * Kijkt na of de schoenmaat bestaand en voegt deze toe aan de sneaker
     * @param size de schoenmaat
     */
    public void setSize(int size) {
        if(size < 35) throw new IllegalArgumentException("Size cannot be below EU 35!");
        else if(size > 50) throw new IllegalArgumentException("Size cannot be above EU 50!");
        else this.size = size;
    }

    public boolean isProfitable() {
        return profitable;
    }

    public void setProfitable(boolean profitable) {
        this.profitable = profitable;
    }


    /**
     * Geeft het Model van de sneaker terug
     *
     * @return een object van het Enum-type Model
     * @see Model model
     */
    public Model getModel() {
        return model;
    }

    /**
     * Setter voor het enum-type Model
     *
     * @param model het model van de sneaker
     * @see Model model
     */
    public void setModel(Model model) {
        this.model = model;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    /**
     * Kijk na of de sneaker gemaakt is na de eerst gemaakt sneaker ooit
     *
     * @param releaseDate de uitgave datum
     */
    public void setReleaseDate(LocalDate releaseDate) {
        if(releaseDate.getYear() < 1980)throw new IllegalArgumentException("Release year cannot be before 1980");
        else this.releaseDate = LocalDate.of(releaseDate.getYear(), releaseDate.getMonth(),releaseDate.getDayOfMonth());
    }

    /**
     * Methode om na te kijken of een Sneaker object gelijk is aan een ander Sneaker object
     *
     * @param o Object van Sneaker
     * @return een boolean met gelijkheid tussen objecten
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sneaker sneaker = (Sneaker) o;
        return name.equals(sneaker.name);
    }

    /**
     * Methode die de hashcode van een object teruggeeft
     * @return int de hashcode
     */
    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    /**
     * Methode om Sneakers op naam te vergelijken
     * @param o sneaker die we moeten te vergelijken
     * @return een int 1 of 0 afhankelijk van de gelijkheid
     */
    @Override
    public int compareTo(Sneaker o) {
        return this.name.compareToIgnoreCase(o.getName());
    }
}
