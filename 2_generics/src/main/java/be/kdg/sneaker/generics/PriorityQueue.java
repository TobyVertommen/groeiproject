package be.kdg.sneaker.generics;

import java.util.*;

public class PriorityQueue<T> implements FIFOqueue<T> {

    private TreeMap<Integer, LinkedList<T>> priorityMap = new TreeMap<>(Comparator.reverseOrder());

    @Override
    public boolean enqueue(T element, int priority) {
        boolean present = false;
        //check aanwezigheid
        for (Map.Entry<Integer, LinkedList<T>> entry : priorityMap.entrySet()) {
            if (entry.getKey().equals(priority)) {
                for (T value : entry.getValue()) {
                    if (value.equals(element)) {
                        present = true;
                        break;
                    }
                }
            }

        }

        if (present) {
            return false;
        } else {
            if (priorityMap.containsKey(priority)) {
                priorityMap.get(priority).addLast(element);
                return true;
            } else {
                priorityMap.put(priority, new LinkedList<T>(List.of(element)));
                return true;
            }
        }

    }

    @Override
    public T dequeue() {
        int max = Collections.max(priorityMap.keySet());
        T removable = priorityMap.get(max).get(0);

        priorityMap.get(max).remove(removable);


        // Integer ook verwijderen uit Treemap
        if (priorityMap.get(max).size() == 0) {
            priorityMap.remove(max);
        }

        return removable;
    }

    @Override
    public int search(T element) {
        int position = 1;
        for (Map.Entry<Integer, LinkedList<T>> entry : priorityMap.entrySet()) {
            for (T value : entry.getValue()) {
                if (value.equals(element)) {
                    return position;
                }
                position++;
            }
        }
        return -1;
    }

    @Override
    public int getSize() {
        int count = 0;
        for (Map.Entry<Integer, LinkedList<T>> entry : priorityMap.entrySet()) {


            for (T value : entry.getValue()) count++;

        }
        return count;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<Integer, LinkedList<T>> entry : priorityMap.entrySet()) {

            for (T value : entry.getValue()) sb.append(entry.getKey()).append(": ").append(value).append("\n");

        }
        return sb.toString();

    }
}
