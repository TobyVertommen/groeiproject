package be.kdg.sneaker.generics;

public interface FIFOqueue<T> {
    boolean enqueue(T element, int priority);

    T dequeue();

    int search(T element);

    int getSize();
}
