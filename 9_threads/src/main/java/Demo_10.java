import be.kdg.sneaker.model.Model;
import be.kdg.sneaker.model.Sneaker;
import be.kdg.sneaker.model.SneakerFactory;
import be.kdg.sneaker.threading.SneakerAttacker;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Demo_10 {
    public static void main(String[] args) {
        List<Sneaker> list = Stream.generate(SneakerFactory::newRandomSneaker).limit(1000).collect(Collectors.toList());

        System.out.println("Before purge: ");
        System.out.println("Amount of low's: "+list.stream().filter(sneaker -> sneaker.getModel().equals(Model.LOW)).count());
        System.out.println("Amount of profitable sneakers: "+list.stream().filter(Sneaker::isProfitable).count());
        System.out.println("Amount of sneakers in size 40: "+list.stream().filter(sneaker -> sneaker.getSize() == 40).count());

        SneakerAttacker attOne = new SneakerAttacker(list, sneaker -> sneaker.getModel().equals(Model.LOW));
        SneakerAttacker attTwo = new SneakerAttacker(list, Sneaker::isProfitable);
        SneakerAttacker attThree = new SneakerAttacker(list, sneaker -> sneaker.getSize() == 40);


        Thread threadOne = new Thread(attOne);
        Thread threadTwo = new Thread(attTwo);
        Thread threadThree = new Thread(attThree);

        System.out.println("\nStarting threads...\n");

        threadOne.start();
        threadTwo.start();
        threadThree.start();

        try {
            threadOne.join();
            threadTwo.join();
            threadThree.join();
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("After purge: ");
        System.out.println("Amount of low's: "+list.stream().filter(sneaker -> sneaker.getModel().equals(Model.LOW)).count());
        System.out.println("Amount of profitable sneakers: "+list.stream().filter(Sneaker::isProfitable).count());
        System.out.println("Amount of sneakers in size 40: "+list.stream().filter(sneaker -> sneaker.getSize() == 40).count());


    }
}
