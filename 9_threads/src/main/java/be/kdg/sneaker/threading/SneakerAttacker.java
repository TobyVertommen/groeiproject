package be.kdg.sneaker.threading;

import be.kdg.sneaker.model.Sneaker;


import java.util.List;
import java.util.function.Predicate;


public class SneakerAttacker implements Runnable {
    private List<Sneaker> sneakers;
    private Predicate<Sneaker> predicate;
    private static final Object LOCK = new Object();


    public SneakerAttacker(List<Sneaker> sneakerse, Predicate<Sneaker> predicatee) {
        sneakers = sneakerse;
        predicate = predicatee;
    }

    @Override
    public void run() {
        synchronized (LOCK) {
            sneakers.removeIf(predicate);
        }
    }

    public List<Sneaker> getSneakers() {
        return sneakers;
    }
}
