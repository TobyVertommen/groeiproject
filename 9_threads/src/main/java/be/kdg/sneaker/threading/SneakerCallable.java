package be.kdg.sneaker.threading;

import be.kdg.sneaker.model.Sneaker;
import be.kdg.sneaker.model.SneakerFactory;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SneakerCallable implements Callable<List<Sneaker>> {
    private Predicate<Sneaker> predicate;
    private List<Sneaker> sneakers;

    public SneakerCallable(Predicate<Sneaker> predicate) {
        this.predicate = predicate;
    }

    @Override
    public List<Sneaker> call() throws Exception {
        return Stream.generate(SneakerFactory::newRandomSneaker).filter(predicate).limit(1000).collect(Collectors.toList());
    }

    public List<Sneaker> getSneakers() {
        return sneakers;
    }
}
