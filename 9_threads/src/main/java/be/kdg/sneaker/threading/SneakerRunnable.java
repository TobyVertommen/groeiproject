package be.kdg.sneaker.threading;

import be.kdg.sneaker.model.Sneaker;
import be.kdg.sneaker.model.SneakerFactory;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SneakerRunnable implements Runnable{
    private Predicate<Sneaker> predicate;
    private List<Sneaker> sneakers;

    public SneakerRunnable(Predicate<Sneaker> predicatee) {
        predicate = predicatee;
    }

    @Override
    public void run() {
       sneakers = Stream.generate(SneakerFactory::newRandomSneaker).filter(predicate).limit(1000).collect(Collectors.toList());
    }

    public List<Sneaker> getSneakers() {
        return sneakers;
    }
}
