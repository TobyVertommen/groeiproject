package be.kdg.sneaker.model;

import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;

public class Sneakers {
    public ArrayBlockingQueue<Sneaker> sneakerSet;


    public Sneakers(int size) {
        sneakerSet = new ArrayBlockingQueue<Sneaker>(size);
    }

    public boolean add(Sneaker sneaker) {
        try {
            sneakerSet.put(sneaker);
            return true;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean remove(String name) {
        for (Sneaker temp : sneakerSet) {
            if (temp.getName().equals(name)) {
                sneakerSet.remove(temp);
                return true;
            }
        }
        return false;
    }

    public Sneaker search(String name) {
        Sneaker s = null;
        for (Sneaker sneaker : sneakerSet) {
            if (sneaker.getName().equals(name)) {
                s = sneaker;
            }
        }
        return s;
    }

    public List<Sneaker> sortedOnName() {
        List<Sneaker> sortedOnNameList = new ArrayList<>(sneakerSet);
        Collections.sort(sortedOnNameList);
        return sortedOnNameList;
    }

    public List<Sneaker> sortedOnPrice() {
        class ByPrice implements Comparator<Sneaker> {
            @Override
            public int compare(Sneaker o1, Sneaker o2) {
                return Double.compare(o1.getPrice(), o2.getPrice());
            }
        }
        List<Sneaker> sortedOnPriceList = new ArrayList<>(sneakerSet);
        sortedOnPriceList.sort(new ByPrice());
        return sortedOnPriceList;
    }

    public List<Sneaker> sortedOnSize() {
        class BySize implements Comparator<Sneaker> {
            @Override
            public int compare(Sneaker o1, Sneaker o2) {
                return o1.getSize() - o2.getSize();
            }
        }
        List<Sneaker> sortedOnSizeList = new ArrayList<>(sneakerSet);
        sortedOnSizeList.sort(new BySize());
        return sortedOnSizeList;
    }


    public int getSize() {
        int count = 0;
        for (Sneaker sneaker : sneakerSet) {
            count++;
        }
        return count;
    }
}
