package be.kdg.sneaker.model;

import java.time.LocalDate;
import java.util.Objects;

public final class Sneaker implements Comparable<Sneaker> {
    private final String name;
    private final double price;
    private final int size;
    private final boolean profitable;
    private final Model model;
    private final LocalDate releaseDate;


    public Sneaker(String namee, double pricee, int sizee, boolean profitablee, Model modele, LocalDate releaseDatee) {
       name = namee;
       price = pricee;
       size = sizee;
       profitable = profitablee;
       model = modele;
       releaseDate = releaseDatee;
    }

    public Sneaker() {
        this.name = "Unknown";
        this.price = 1;
        this.size = 35;
        this.profitable = false;
        this.model = Model.LOW;
        this.releaseDate = LocalDate.of(1980, 1, 1);
    }

    public String toString() {
        return String.format("\n%-100s %15.2feur %10dEU %15s   profit = %-5b %15s", this.getName(),
                this.getPrice(),
                this.getSize(),
                this.getModel(),
                this.isProfitable(),
                this.getReleaseDate().toString());
    }

    public String getName() {
        return name;
    }


    public double getPrice() {
        return price;
    }


    public int getSize() {
        return size;
    }


    public boolean isProfitable() {
        return profitable;
    }


    public Model getModel() {
        return model;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sneaker sneaker = (Sneaker) o;
        return name.equals(sneaker.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public int compareTo(Sneaker o) {
        return this.name.compareToIgnoreCase(o.getName());
    }


}
