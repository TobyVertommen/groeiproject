import be.kdg.sneaker.model.Model;
import be.kdg.sneaker.model.Sneaker;
import be.kdg.sneaker.threading.SneakerRunnable;

import java.time.LocalDate;

public class Demo_9 {
    public static void main(String[] args) {
        int TESTCOUNT = 0;
        long average = 0;
        SneakerRunnable runOne = new SneakerRunnable(sneaker -> sneaker.getModel().equals(Model.HIGH));
        SneakerRunnable runTwo = new SneakerRunnable(Sneaker::isProfitable);
        SneakerRunnable runThree = new SneakerRunnable(sneaker -> sneaker.getReleaseDate().isBefore(LocalDate.of(2001, 1, 1)));


        while (100 > TESTCOUNT) {
            long start = System.currentTimeMillis();

            Thread threadOne = new Thread(runOne);
            Thread threadTwo = new Thread(runTwo);
            Thread threadThree = new Thread(runThree);

            threadOne.start();
            threadTwo.start();
            threadThree.start();

            try {
                threadOne.join();
                threadTwo.join();
                threadThree.join();
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }

            runOne.getSneakers().stream().limit(5).forEach(System.out::println);
            runTwo.getSneakers().stream().limit(5).forEach(System.out::println);
            runThree.getSneakers().stream().limit(5).forEach(System.out::println);

            long total = System.currentTimeMillis() - start;
            average += total;

            TESTCOUNT++;
        }

        long finalAv = average/TESTCOUNT;
        System.out.println("3 threads verzamelen elk 1000 Sneakers (gemiddelde uit 100 runs): "+finalAv+" ms.");
    }

}
