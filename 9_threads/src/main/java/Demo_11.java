import be.kdg.sneaker.model.Model;
import be.kdg.sneaker.model.Sneaker;
import be.kdg.sneaker.threading.SneakerCallable;

import java.util.List;
import java.util.concurrent.*;

public class Demo_11 {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        int TESTCOUNT = 0;
        long average = 0;
        ExecutorService executor = Executors.newFixedThreadPool(3);

        while (100 > TESTCOUNT) {
            long start = System.currentTimeMillis();

            Callable<List<Sneaker>> callOne = new SneakerCallable(sneaker -> sneaker.getModel().equals(Model.LOW));
            Future<List<Sneaker>> futOne = executor.submit(callOne);
            futOne.get();

            Callable<List<Sneaker>> callTwo = new SneakerCallable(Sneaker::isProfitable);
            Future<List<Sneaker>> futTwo = executor.submit(callTwo);
            futTwo.get();

            Callable<List<Sneaker>> callThree = new SneakerCallable(sneaker -> sneaker.getSize() == 40);
            Future<List<Sneaker>> futThree = executor.submit(callThree);
            futThree.get();


            long total = System.currentTimeMillis() - start;
            average += total;

            TESTCOUNT++;
        }
        executor.shutdown();

        long finalAv = average/TESTCOUNT;
        System.out.println("3 futures verzamelen elk 1000 Sneakers (gemiddelde uit 100 runs): "+finalAv+" ms.");
    }
}
