import be.kdg.sneaker.data.Data;
import be.kdg.sneaker.model.SneakerFactory;
import be.kdg.sneaker.model.Sneakers;
import be.kdg.sneaker.threading.SneakerRunnable;

import java.util.stream.Stream;

public class Demo_12 {
    public static void main(String[] args) {
        Sneakers sneakers = new Sneakers(10000);
        Runnable run = () -> {
            Stream.generate(SneakerFactory::newRandomSneaker).limit(5000).forEach(sneakers.sneakerSet::add);
        };

        Thread threadOne = new Thread(run);
        Thread threadTwo = new Thread(run);

        threadOne.start();
        threadTwo.start();

        try {
            threadOne.join();
            threadTwo.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Na toevoegen door 2 threads met elk 5000 objecten: sneakers = "+sneakers.getSize() );
    }
}
