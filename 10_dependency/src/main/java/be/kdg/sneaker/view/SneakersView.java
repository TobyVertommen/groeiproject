package be.kdg.sneaker.view;

import be.kdg.sneaker.model.Sneaker;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;

import java.util.logging.Logger;

public class SneakersView extends BorderPane {
    private TableView tvSneakers;
    private TextField  tfName;
    private TextField tfPrice;
    private DatePicker dpReleaseDate;
    private Button btnSave;
    private static final Logger logger = Logger.getLogger(SneakersView.class.getName());

    public SneakersView(){
        logger.info("Creating SneakersView..");

        tvSneakers = new TableView<>();

        tfName = new TextField();
        tfName.setPromptText("Name");

        tfPrice = new TextField();
        tfPrice.setPromptText("Price");

        dpReleaseDate = new DatePicker();
        dpReleaseDate.setPromptText("Release");
        btnSave = new Button("Save");

        super.setCenter(tvSneakers);

        BorderPane.setMargin(tvSneakers, new Insets(10));

        TableColumn<String, Sneaker> column1 = new TableColumn<>("Name");
        column1.setCellValueFactory(new PropertyValueFactory<>("name"));

        TableColumn<String, Sneaker> column2 = new TableColumn<>("Price");
        column2.setCellValueFactory(new PropertyValueFactory<>("price"));

        TableColumn<String, Sneaker> column3 = new TableColumn<>("Release");
        column3.setCellValueFactory(new PropertyValueFactory<>("releaseDate"));

        tvSneakers.getColumns().addAll(column1, column2, column3);

        HBox bottom = new HBox(tfName, tfPrice, dpReleaseDate, btnSave);
        super.setBottom(bottom);
        BorderPane.setMargin(bottom, new Insets(10));

    }

    TableView<Sneaker> getTvSneakers() {
        return tvSneakers;
    }

    TextField getTfName() {
        return tfName;
    }

    TextField getTfPrice() {
        return tfPrice;
    }

    DatePicker getDpReleaseDate() {
        return dpReleaseDate;
    }

    Button getBtnSave() {
        return btnSave;
    }
}
