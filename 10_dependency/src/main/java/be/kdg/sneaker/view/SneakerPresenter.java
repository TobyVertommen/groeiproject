package be.kdg.sneaker.view;

import be.kdg.sneaker.exceptions.SneakerException;
import be.kdg.sneaker.model.Sneaker;
import be.kdg.sneaker.service.SneakerService;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;

import java.util.List;
import java.util.logging.Logger;

public class SneakerPresenter {
    private static final Logger logger = Logger.getLogger(SneakerPresenter.class.getName());
    private SneakersView sneakersView;
    private SneakerService sneakersService;


    public SneakerPresenter(SneakersView sneakersView, SneakerService sneakersService) {
        this.sneakersView = sneakersView;
        this.sneakersService = sneakersService;

        loadSneakers();

        sneakersView.getBtnSave().setOnAction(event -> {
            try {
                Sneaker sneaker = new Sneaker(sneakersView.getTfName().getText(), Double.parseDouble(sneakersView.getTfPrice().getText()), sneakersView.getDpReleaseDate().getValue());
                sneaker.setId(-1);
                sneakersService.addSneaker(sneaker);
            } catch (IllegalArgumentException e) {
                logger.warning("Unable to add sneaker: " + e.getMessage());
                new Alert(Alert.AlertType.ERROR, "Unable to add sneaker:\n" + e.getMessage()).showAndWait();
            }
            loadSneakers();
        });
    }

    private void loadSneakers() {
        logger.info("Loading sneakers...");
        try {
            List<Sneaker> list = sneakersService.getAllSneakers();
            sneakersView.getTvSneakers().setItems(FXCollections.observableList(list));
        } catch (SneakerException e) {
            logger.warning("Unable to load sneakers: " + e.getMessage());
            new Alert(Alert.AlertType.ERROR, "Unable to load sneakers:\n" + e.getMessage()).showAndWait();
        }


    }
}
