package be.kdg.sneaker.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

public class Sneaker implements Comparable<Sneaker>, Serializable  {
    private static final long serialVersionUID = 1L;
    private String name;
    private double price;
    private transient int size;
    private transient boolean profitable;
    private transient Model model;
    private LocalDate releaseDate;

    private int id;


    public Sneaker(String name, double price, int size,boolean profitable, Model model, LocalDate releaseDate) {
        this(name, price, size, profitable, model, releaseDate, -1);
    }

    public Sneaker(String name, double price, int size,boolean profitable, Model model, LocalDate releaseDate, int id) {
        setName(name);
        setPrice(price);
        setSize(size);
        setProfitable(profitable);
        setModel(model);
        setReleaseDate(releaseDate);
        setId(id);
    }

    public Sneaker() {
     new Sneaker("Unknown", 1, 35,false, Model.LOW, LocalDate.of(1980,1,1));
    }

    public Sneaker(String namee, double pricee, LocalDate releaseDatee) {
        name = namee;
        price = pricee;
        releaseDate = releaseDatee;
        profitable = false;
        size = 36;
        model = Model.LOW;
    }

//    public String toString() {
//        return String.format("\n%-30s %15.2feur %10dEU %15s   profit = %-5b %15s", this.getName(),
//                this.getPrice(),
//                this.getSize(),
//                this.getModel(),
//                this.isProfitable(),
//                this.getReleaseDate().toString());
//    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name == null) throw new IllegalArgumentException("Name cannot be empty!");
        else this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        if (price <= 0) throw new IllegalArgumentException("Price cannot be below 0 or equal to 0!");
        else this.price = price;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        if(size < 35) throw new IllegalArgumentException("Size cannot be below EU 35!");
        else if(size > 50) throw new IllegalArgumentException("Size cannot be above EU 50!");
        else this.size = size;
    }

    public boolean isProfitable() {
        return profitable;
    }

    public void setProfitable(boolean profitable) {
        this.profitable = profitable;
    }




    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        if(releaseDate.getYear() < 1980)throw new IllegalArgumentException("Release year cannot be before 1980");
        else this.releaseDate = LocalDate.of(releaseDate.getYear(), releaseDate.getMonth(),releaseDate.getDayOfMonth());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sneaker sneaker = (Sneaker) o;
        return name.equalsIgnoreCase(sneaker.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public int compareTo(Sneaker o) {
        return this.getName().compareToIgnoreCase(o.getName());
    }


}
