package be.kdg.sneaker.service;

import be.kdg.sneaker.database.SneakerDao;

import be.kdg.sneaker.model.Sneaker;

import java.util.List;

public class SneakerServiceImpl implements SneakerService {
    private SneakerDao sneakerDao;

    public SneakerServiceImpl(SneakerDao sneakerDao) {
        this.sneakerDao = sneakerDao;
    }

    @Override
    public List<Sneaker> getAllSneakers() {
        return sneakerDao.getAllSneakers();
    }

    @Override
    public void addSneaker(Sneaker sneaker) {
        sneakerDao.insert(sneaker);
    }
}
