package be.kdg.sneaker.service;

import be.kdg.sneaker.model.Sneaker;

import java.util.List;

public interface SneakerService {
    List<Sneaker> getAllSneakers();
    void addSneaker(Sneaker sneaker);
}
