package be.kdg.sneaker.database;

import be.kdg.sneaker.model.Sneaker;

import java.util.List;

public interface SneakerDao {
    void insert(Sneaker sneaker);
    boolean delete(String name);
    boolean update(Sneaker sneaker);
    Sneaker retrieve(String name);
    List<Sneaker> sortedOn(String query);
    List<Sneaker> getAllSneakers();
}
