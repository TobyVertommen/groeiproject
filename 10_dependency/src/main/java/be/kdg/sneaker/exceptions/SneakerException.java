package be.kdg.sneaker.exceptions;

public class SneakerException extends RuntimeException {
    public SneakerException(String message) { super(message); }
    public SneakerException(Throwable cause) { super(cause);}
}
