package be.kdg.sneaker;

import be.kdg.sneaker.database.SneakerDao;
import be.kdg.sneaker.database.SneakerDbDao;
import be.kdg.sneaker.service.SneakerService;
import be.kdg.sneaker.service.SneakerServiceImpl;
import be.kdg.sneaker.view.SneakerPresenter;
import be.kdg.sneaker.view.SneakersView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.logging.Logger;

public class Main extends Application {
    private static final Logger logger = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {
        logger.info("Starting main...");
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        logger.info("Starting application on thread: "+Thread.currentThread().getName());

        SneakerDao sneakerDao = SneakerDbDao.getInstance();
        SneakersView sneakersView = new SneakersView();
        SneakerService sneakerService = new SneakerServiceImpl(sneakerDao);

        new SneakerPresenter(sneakersView, sneakerService);

        primaryStage.setScene(new Scene(sneakersView));
        primaryStage.show();
    }
}
