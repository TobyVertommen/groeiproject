package be.kdg.sneaker.data;

import be.kdg.sneaker.model.Model;
import be.kdg.sneaker.model.Sneaker;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

public class Data {


    private static final Sneaker s1 = new Sneaker("Air Jordan 1 Cactus Jack", 350, 43, true, Model.HIGH, LocalDate.of(1980, 3, 21));
    private static final Sneaker s2 = new Sneaker("Air Jordan 1 Travis Scott", 400, 41, true, Model.HIGH, LocalDate.of(1990, 5, 11));
    private static final Sneaker s3 = new Sneaker("Air Jordan 4 Oreo", 200, 44, true, Model.MID, LocalDate.of(2011, 3, 30));
    private static final Sneaker s4 = new Sneaker("Air Force 1 Undefeated", 250, 40, false, Model.LOW, LocalDate.of(2018, 1, 21));
    private static final Sneaker s5 = new Sneaker("Dunk Paisley", 500, 43, true, Model.LOW, LocalDate.of(2000, 3, 1));
    private static final Sneaker s6 = new Sneaker("Dunk University Red", 150, 39, false, Model.LOW, LocalDate.of(2002, 7, 4));
    private static final Sneaker s7 = new Sneaker("Air Force 1 Bandana", 180, 45, true, Model.LOW, LocalDate.of(2020, 6, 9));
    private static final Sneaker s8 = new Sneaker("Blazer Off-White", 220, 39, true, Model.MID, LocalDate.of(2005, 8, 8));
    private static final Sneaker s9 = new Sneaker("Vapormax Supreme", 290, 44, false, Model.LOW, LocalDate.of(2014, 9, 6));
    private static final Sneaker s10 = new Sneaker("Air Jordan 1 Off-White", 650, 43, true, Model.HIGH, LocalDate.of(1988, 2, 27));
    private static final Sneaker s11 = new Sneaker("Dunk Off-White", 610, 40, true, Model.LOW, LocalDate.of(1999, 4, 23));
    private static final Sneaker s12 = new Sneaker("Air Jordan 4 University Blue", 440, 42, true, Model.MID, LocalDate.of(2021, 3, 3));
    private static final Sneaker s13 = new Sneaker("Air Jordan 4 Sail ", 200, 39, true, Model.MID, LocalDate.of(2019, 3, 26));
    private static final Sneaker s15 = new Sneaker("Vans SK8 hi", 80, 45, false, Model.HIGH, LocalDate.of(2001, 5, 14));
    private static final Sneaker s16 = new Sneaker("Vans SK8 lo", 80, 42, false, Model.LOW, LocalDate.of(2001, 5, 14));
    private static final Sneaker s17 = new Sneaker("Vans SK8 mi", 80, 41, false, Model.MID, LocalDate.of(2001, 5, 14));


    public static List<Sneaker> getData() {
        List<Sneaker> fullList = new LinkedList<>();

        fullList.add(s1);
        fullList.add(s2);
        fullList.add(s3);
        fullList.add(s4);
        fullList.add(s5);
        fullList.add(s6);
        fullList.add(s7);
        fullList.add(s8);
        fullList.add(s9);
        fullList.add(s10);
        fullList.add(s11);
        fullList.add(s12);
        fullList.add(s13);
        fullList.add(s15);
        fullList.add(s16);
        fullList.add(s17);

        return fullList;
    }


}
