import be.kdg.sneaker.model.Model;
import be.kdg.sneaker.model.Shoe;
import be.kdg.sneaker.model.Sneaker;
import be.kdg.sneaker.reflection.ReflectionTools;

import java.lang.reflect.InvocationTargetException;
import java.time.LocalDate;

public class Demo_3 {
    public static void main(String[] args) {
        System.out.println("3 build working!!!");
        Sneaker sn = new Sneaker("AJ1 Cactus Jack", 350, 43, true, Model.HIGH, LocalDate.of(1980,3,21));
        try {
            System.out.println(ReflectionTools.runAnnotated(Shoe.class));
        } catch (InstantiationException | NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
