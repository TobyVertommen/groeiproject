package be.kdg.sneaker.model;

import be.kdg.sneaker.reflection.CanRun;

import java.time.LocalDate;
import java.util.Objects;

public class Shoe implements Comparable<Shoe>{
    private String name;
    private double price;
    private int size;
    private LocalDate releaseDate;

    public Shoe(String namee, double pricee, int sizee, LocalDate releaseDatee) {
        name = namee;
        price = pricee;
        size = sizee;
        releaseDate = releaseDatee;
    }


    public Shoe(){
        name = "Default";
        price = 1;
        size = 35;
        releaseDate  = LocalDate.now();
    }



    public String getName() {
        return name;
    }

    @CanRun("Tobes Scott")
    public void setName(String name) {
        if (name == null) throw new IllegalArgumentException("Name cannot be empty!");
        else this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        if (price <= 0) throw new IllegalArgumentException("Price cannot be below 0 or equal to 0!");
        else this.price = price;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        if (size < 35) throw new IllegalArgumentException("Size cannot be below EU 35!");
        else if (size > 50) throw new IllegalArgumentException("Size cannot be above EU 50!");
        else this.size = size;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        if (releaseDate.getYear() < 1980) throw new IllegalArgumentException("Release year cannot be before 1980");
        else
            this.releaseDate = LocalDate.of(releaseDate.getYear(), releaseDate.getMonth(), releaseDate.getDayOfMonth());
    }

    public String toString() {
        return String.format("\n%-30s %15.2feur %10dEU %15s", this.getName(),
                this.getPrice(),
                this.getSize(),

                this.getReleaseDate().toString());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Shoe shoe = (Shoe) o;
        return name.equals(shoe.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }


    @Override
    public int compareTo(Shoe o) {
        return this.name.compareToIgnoreCase(o.getName());
    }
}
