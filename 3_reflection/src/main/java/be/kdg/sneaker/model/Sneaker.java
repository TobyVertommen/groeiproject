package be.kdg.sneaker.model;

import java.time.LocalDate;
import java.util.Objects;

public class Sneaker extends Shoe{

    private boolean profitable;
    private Model model;



    public Sneaker(String name, double price, int size,boolean profitable, Model model, LocalDate releaseDate) {
        super(name, price, size, releaseDate);
        setProfitable(profitable);
        setModel(model);

    }

//    public Sneaker() {
//     new Sneaker("Unknown", 1, 35,false, Model.LOW, LocalDate.of(1980,1,1));
//    }

    public String toString() {
        return super.toString()+String.format(" profit = %-5b %15s",
                this.isProfitable(),
                this.getModel());
    }

    public boolean isProfitable() {
        return profitable;
    }

    public void setProfitable(boolean profitable) {
        this.profitable = profitable;
    }




    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }



}
