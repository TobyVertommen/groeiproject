package be.kdg.sneaker.model;

import java.util.*;

public class Sneakers {
    public TreeSet<Sneaker> sneakerSet;


    public Sneakers(List<Sneaker> list) {
        sneakerSet = new TreeSet<Sneaker>(list);
    }

    public boolean add(Sneaker sneaker) {
        return this.sneakerSet.add(sneaker);
    }

    public void remove(String name){
        Sneaker found = null;
        for (Sneaker sneaker : sneakerSet) {
            if(sneaker.getName().equals(name)){
                found = sneaker;
            }
        }

        if (found != null) sneakerSet.remove(found);
        else System.out.println("Sneaker not in list!");
    }

    public Sneaker search(String name) {
        Sneaker s = null;
        for (Sneaker sneaker : sneakerSet) {
            if (sneaker.getName().equals(name)) {
                s = sneaker;
            }
        }
        return s;
    }

    public List<Sneaker> sortedOnName() {
        List<Sneaker> sortedOnNameList = new ArrayList<>(sneakerSet);
        Collections.sort(sortedOnNameList);
        return sortedOnNameList;
    }

    public List<Sneaker> sortedOnPrice() {
        class ByPrice implements Comparator<Sneaker> {
            @Override
            public int compare(Sneaker o1, Sneaker o2) {
                return Double.compare(o1.getPrice(), o2.getPrice());
            }
        }
        List<Sneaker> sortedOnPriceList = new ArrayList<>(sneakerSet);
        sortedOnPriceList.sort(new ByPrice());
        return sortedOnPriceList;
    }

    public List<Sneaker> sortedOnSize() {
        class BySize implements Comparator<Sneaker> {
            @Override
            public int compare(Sneaker o1, Sneaker o2) {
                return o1.getSize()- o2.getSize();
            }
        }
        List<Sneaker> sortedOnSizeList = new ArrayList<>(sneakerSet);
        sortedOnSizeList.sort(new BySize());
        return sortedOnSizeList;
    }


    public int getSize() {
        int count = 0;
        for (Sneaker sneaker : sneakerSet) {
            count++;
        }
        return count;
    }
}
