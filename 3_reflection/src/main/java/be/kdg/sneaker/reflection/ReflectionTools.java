package be.kdg.sneaker.reflection;

import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.util.Arrays;

public class ReflectionTools {


    public static void classAnalysis(Class aClass) {
        System.out.println("Analyse van de klasse: " + aClass.getSimpleName());
        System.out.println("============================================================");
        System.out.println("Fully qualified name: " + aClass.getName());
        System.out.println("Naam van de superklasse: " + aClass.getSuperclass().getSimpleName());
        System.out.println("Naam van de package:" +aClass.getPackage().getName());
        System.out.println("Interfaces: " + Arrays.toString(aClass.getInterfaces()));
       // System.out.println("Constructors: " + Arrays.toString(aClass.getDeclaredConstructors()));
        System.out.println("Constructors: " + aClass.toGenericString());

        System.out.print("attributen: ");
        for (Field field : aClass.getDeclaredFields()) {
            System.out.printf("%s(%s) ", field.getName(), field.getType());
        }
        System.out.print("\ngetters: ");
        for (Method method : aClass.getDeclaredMethods()) {
            if (method.getName().startsWith("get")) {
                System.out.printf("%s ", method.getName());
            }
        }

        System.out.print("\nsetters: ");
        for (Method method : aClass.getDeclaredMethods()) {
            if (method.getName().startsWith("set")) {
                System.out.printf("%s ", method.getName());
            }
        }

        System.out.print("\nandere methoden: ");
        for (Method method : aClass.getDeclaredMethods()) {
            if (!method.getName().startsWith("set") && !method.getName().startsWith("get")) {
                System.out.printf("%s ", method.getName());
            }
        }
    }

    public static Object runAnnotated(Class aClass) throws InstantiationException ,NoSuchMethodException , InvocationTargetException, IllegalAccessException{
        var temp = aClass.getDeclaredConstructor().newInstance();
            for (Method method : aClass.getDeclaredMethods()) {
                if (method.getGenericParameterTypes().length == 1){
                    if(Arrays.equals(method.getGenericParameterTypes(), new Class[]{String.class})
                            && method.isAnnotationPresent(CanRun.class)){
                        method.invoke(temp, method.getDeclaredAnnotation(CanRun.class).value());
                    }
                }

            }
        return temp;
    }

}
