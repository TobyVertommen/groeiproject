import be.kdg.sneaker.model.Model;
import be.kdg.sneaker.model.Sneaker;
import be.kdg.sneaker.model.Sneakers;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.logging.LogManager;

public class Demo_5 {
    public static void main(String[] args) {
        loadLoggingConfiguration();
        Sneaker s1 = new Sneaker("Air Jordan 1 Cactus Jack", 350, 33, true, Model.HIGH, LocalDate.of(1980, 3, 21));
        Sneaker s2 = new Sneaker(null, 400, 41, true, Model.HIGH, LocalDate.of(1990, 5, 11));
        Sneaker s3 = new Sneaker("Air Jordan 4 Oreo", 200, 44, true, Model.MID, LocalDate.of(1970, 3, 30));
        Sneaker s4 = new Sneaker("Air Force 1 Undefeated", -50, 40, false, Model.LOW, LocalDate.of(2018, 1, 21));

        InputStream in = Demo_5.class.getResourceAsStream("logging.properties");

        Sneakers ss = new Sneakers();
        Sneaker s5 = new Sneaker("Dunk Paisley", 500, 43, true, Model.LOW, LocalDate.of(2000, 3, 1));


        ss.add(s5);
        ss.remove(s5.getName());

    }

    private static void loadLoggingConfiguration() {
        InputStream inputStream = Demo_5.class.getResourceAsStream("logging.properties");
        try {
            LogManager.getLogManager().readConfiguration(inputStream);
        } catch(IOException e) {
            System.err.println("Logging config can't be loaded!");
        }
    }
}
