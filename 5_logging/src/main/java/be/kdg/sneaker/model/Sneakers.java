package be.kdg.sneaker.model;

import java.util.*;
import java.util.logging.Logger;

public class Sneakers {
    public TreeSet<Sneaker> sneakerSet;
    private static final Logger logger = Logger.getLogger("be.kdg.model.sneakers");


    public Sneakers(List<Sneaker> list) {
        sneakerSet = new TreeSet<Sneaker>(list);
    }

    public Sneakers() {
        sneakerSet = new TreeSet<Sneaker>();
    }

    public boolean add(Sneaker sneaker) {
        logger.finer(sneaker.getName()+" is being added to the sneaker-set!");
        return this.sneakerSet.add(sneaker);
    }

    public boolean remove(String name) {
        logger.finer(name+" is being removed from the sneaker-set!");
        for (Sneaker temp : sneakerSet) {
            if (temp.getName().equals(name)) {
                sneakerSet.remove(temp);
                return true;
            }
        }
        return false;
    }

    public Sneaker search(String name) {
        Sneaker s = null;
        for (Sneaker sneaker : sneakerSet) {
            if (sneaker.getName().equals(name)) {
                s = sneaker;
            }
        }
        return s;
    }

    public List<Sneaker> sortedOnName() {
        List<Sneaker> sortedOnNameList = new ArrayList<>(sneakerSet);
        Collections.sort(sortedOnNameList);
        return sortedOnNameList;
    }

    public List<Sneaker> sortedOnPrice() {
        class ByPrice implements Comparator<Sneaker> {
            @Override
            public int compare(Sneaker o1, Sneaker o2) {
                return Double.compare(o1.getPrice(), o2.getPrice());
            }
        }
        List<Sneaker> sortedOnPriceList = new ArrayList<>(sneakerSet);
        sortedOnPriceList.sort(new ByPrice());
        return sortedOnPriceList;
    }

    public List<Sneaker> sortedOnSize() {
        class BySize implements Comparator<Sneaker> {
            @Override
            public int compare(Sneaker o1, Sneaker o2) {
                return o1.getSize() - o2.getSize();
            }
        }
        List<Sneaker> sortedOnSizeList = new ArrayList<>(sneakerSet);
        sortedOnSizeList.sort(new BySize());
        return sortedOnSizeList;
    }


    public int getSize() {
        int count = 0;
        for (Sneaker sneaker : sneakerSet) {
            count++;
        }
        return count;
    }
}
