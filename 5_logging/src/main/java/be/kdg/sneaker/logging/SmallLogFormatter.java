package be.kdg.sneaker.logging;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

public class SmallLogFormatter extends Formatter {
    @Override
    public String format(LogRecord record) {
        LocalDateTime date = LocalDateTime.ofInstant(record.getInstant(), ZoneId.systemDefault());
        String mess = MessageFormat.format(record.getMessage(), record.getParameters());
        return String.format("%s %-15s Level: %-10s melding: \"%s\"\n"
                ,date.toLocalDate()
                , date.toLocalTime().format(DateTimeFormatter.ofPattern("hh:mm:ss:SSS"))
                ,record.getLevel().toString(), mess);
    }
}
