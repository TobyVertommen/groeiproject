import be.kdg.sneaker.data.Data;
import be.kdg.sneaker.model.Model;
import be.kdg.sneaker.model.Sneaker;
import be.kdg.sneaker.model.Sneakers;

import java.time.LocalDate;

public class Demo_1 {
    public static void main(String[] args) {



        Sneakers multiSneakers = new Sneakers(Data.getData());

        Sneaker sn = new Sneaker("AJ1 Cactus Jack", 350, 43, true, Model.HIGH, LocalDate.of(1980,3,21));

        multiSneakers.add(sn);

        System.out.println("Found: "+multiSneakers.search("AJ1 Cactus Jack"));

        System.out.println("Removed: "+ multiSneakers.remove("AJ1 Cactus Jack"));

        System.out.println("Size: "+multiSneakers.getSize());


        System.out.println("On size:");
        System.out.println(multiSneakers.sortedOnSize());

        System.out.println("On name:");
        System.out.println(multiSneakers.sortedOnName());

        System.out.println("On price:");
        System.out.println(multiSneakers.sortedOnPrice());




    }


}
