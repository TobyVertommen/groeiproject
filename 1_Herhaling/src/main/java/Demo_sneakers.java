import be.kdg.sneaker.data.Data;
import be.kdg.sneaker.model.Model;
import be.kdg.sneaker.model.Sneaker;
import be.kdg.sneaker.model.Sneakers;

import java.time.LocalDate;

public class Demo_sneakers {
    public static void main(String[] args) {
        Sneaker sn = new Sneaker("AJ1 Cactus Jack", 350, 43, true, Model.HIGH, LocalDate.of(1980,3,21));

        Sneakers multiSneakers = new Sneakers(Data.getData());

        System.out.println(multiSneakers.sortedOnPrice());


    }
}
