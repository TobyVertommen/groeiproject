import be.kdg.sneaker.kollections.lists.ArrayList;
import be.kdg.sneaker.kollections.Kollections;
import be.kdg.sneaker.kollections.lists.LinkedList;
import be.kdg.sneaker.kollections.lists.List;
import be.kdg.sneaker.kollections.maps.HashMap;
import be.kdg.sneaker.kollections.maps.ListMap;
import be.kdg.sneaker.kollections.maps.Map;
import be.kdg.sneaker.kollections.sets.ArraySet;
import be.kdg.sneaker.kollections.sets.Set;
import be.kdg.sneaker.kollections.sets.TreeSet;
import be.kdg.sneaker.model.Sneaker;
import be.kdg.sneaker.model.SneakerFactory;


import java.util.HashSet;
import java.util.Random;
import java.util.stream.Stream;

public class PerformanceTester {

    public static List<Sneaker> randomList(int n) {
        //List<Sneaker> myList = new ArrayList<>();
        List<Sneaker> myList = new LinkedList<>();
        Stream.generate(SneakerFactory::newRandomSneaker).limit(n).forEach(myList::add);
        return myList;
    }

    public static void compareArrayListAndLinkedList(int n) {
        List<Sneaker> arrayList = new ArrayList<>();
        List<Sneaker> linkedList = new LinkedList<>();


        long startA = System.currentTimeMillis();
        new Random().ints(n).forEach(i -> arrayList.add(0, SneakerFactory.newRandomSneaker()));
        long endA = System.currentTimeMillis();
        long duurA = endA - startA;
        System.out.printf("Adding %d to ArrayList: %d ms.%n", n, duurA);


        long startL = System.currentTimeMillis();
        new Random().ints(n).forEach(i -> linkedList.add(0, SneakerFactory.newRandomSneaker()));
        long endL = System.currentTimeMillis();
        long duurL = endL - startL;
        System.out.printf("Adding %d to LinkedList: %d ms.%n", n, duurL);


        startA = System.currentTimeMillis();
        new Random().ints(n).forEach(i -> arrayList.get(arrayList.size() - 1));
        endA = System.currentTimeMillis();
        duurA = endA - startA;
        System.out.printf("Getting %d from ArrayList: %d ms.%n", n, duurA);


        startL = System.currentTimeMillis();
        new Random().ints(n).forEach(i -> linkedList.get(linkedList.size() - 1));
        endL = System.currentTimeMillis();
        duurL = endL - startL;
        System.out.printf("Getting %d from LinkedList: %d ms.%n", n, duurL);

    }

    public static void testSelectionSort() {
        for (int n = 1000; n < 10000; n += 1000) {
            Kollections.selectionSort(randomList(n));
            System.out.printf("%d;%d%n", n, Sneaker.compareCounter);
            Sneaker.compareCounter = 0;
        }
    }

    public static void testMergeSort() {
        for (int n = 1000; n < 10000; n += 1000) {
            Kollections.mergeSort(randomList(n));
            System.out.printf("%d;%d%n", n, Sneaker.compareCounter);
            Sneaker.compareCounter = 0;
        }
    }


    public static void compareListMapToHashMap(int size) {
        Sneaker.equalsCounter = 0;

        ListMap<Sneaker, String> list = new ListMap<>();
        fillMap(size, list);

        HashMap<Sneaker, String> hash = new HashMap<>();
        fillMap(size, hash);

        long startList = System.nanoTime();
        for (int i = 0; i < size; i++) {
            String temp = "Sneaker" + i;
            Sneaker tempS = SneakerFactory.newEmptySneaker();
            tempS.setName(temp);
            list.get(tempS);
        }
        long endList = System.nanoTime();
        long timeList = endList - startList;
        int countList = Sneaker.equalsCounter;
        Sneaker.equalsCounter = 0;


        long startHash = System.nanoTime();
        for (int i = 0; i < size; i++) {
            String temp = "Sneaker" + i;
            Sneaker tempS = SneakerFactory.newEmptySneaker();
            tempS.setName(temp);
            hash.get(tempS);
        }
        long endHash = System.nanoTime();
        long timeHash = endHash - startHash;
        int countHash = Sneaker.equalsCounter;
        Sneaker.equalsCounter = 0;

        System.out.printf("\nListmap: n = %d, equalscount = %d , nanosec = %d", size, countList, timeList);
        System.out.printf("\nHashmap: n = %d, equalscount = %d , nanosec = %d", size, countHash, timeHash);
    }


    public static void compareArraySetToTreeSet(int size) {
        Sneaker.equalsCounter = 0;
        Sneaker.compareCounter = 0;

        Set<Sneaker> array = new ArraySet<>();
        Set<Sneaker> tree = new TreeSet<>();


        long startArray = System.nanoTime();

        Stream.generate(SneakerFactory::newRandomSneaker).limit(size).forEach(array::add);

        long endArray = System.nanoTime();
        long timeArray = endArray - startArray;
        int equalsCountArray = Sneaker.equalsCounter;
        int compareCountArray = Sneaker.compareCounter;

        Sneaker.equalsCounter = 0;
        Sneaker.compareCounter = 0;


        long startTree = System.nanoTime();

        Stream.generate(SneakerFactory::newRandomSneaker).limit(size).forEach(tree::add);


        long endTree = System.nanoTime();
        long timeTree = endTree - startTree;
        int equalsCountTree = Sneaker.equalsCounter;
        int compareCountTree = Sneaker.compareCounter;

        Sneaker.equalsCounter = 0;
        Sneaker.compareCounter = 0;

        System.out.printf("\nArraySet: n = %d, equalscount = %d ", size, equalsCountArray);
        System.out.printf("\nArraySet: n = %d, comparecount = %d ", size, compareCountArray);
        System.out.printf("\nArraySet: n = %d, nanosec = %d ", size, timeArray);

        System.out.printf("\nTreeSet: n = %d, equalscount = %d ", size, equalsCountTree);
        System.out.printf("\nTreeSet: n = %d, comparecount = %d ", size, compareCountTree);
        System.out.printf("\nTreeSet: n = %d, nanosec = %d ", size, timeTree);


    }


    public static void fillMap(int size, Map<Sneaker, String> map) {
        for (int i = 0; i < size; i++) {
            String temp = "Sneaker" + i;
            Sneaker tempS = SneakerFactory.newEmptySneaker();
            tempS.setName(temp);
            map.put(tempS, "Ik ben de waarde van " + temp);
        }
    }
}
