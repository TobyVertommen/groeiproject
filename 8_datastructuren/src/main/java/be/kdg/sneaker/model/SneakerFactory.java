package be.kdg.sneaker.model;

import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class SneakerFactory {


    private SneakerFactory() {

    }

    public static Sneaker newEmptySneaker() {
        return new Sneaker();
    }

    public static Sneaker newFilledSneaker(String name, double price, int size, boolean profitable, Model model, LocalDate releaseDate) {
        return new Sneaker(name, price, size, profitable, model, releaseDate);
    }

    public static Sneaker newRandomSneaker() {

        Random random = new Random();
        return new Sneaker(
                generateString(25, 3, true),
                random.nextDouble()*1000,
                random.nextInt(50 + 1 - 35) + 35,
                random.nextBoolean(),
                Model.values()[new Random().nextInt(Model.values().length)],
                generateDate()
                );
    }


    private static String generateString(int maxLength, int wordCount, boolean camelCase) {
        int words = new Random().nextInt(wordCount)+1;
        StringBuilder sb = new StringBuilder();
        String klinker = "aeyuio";
        String medeklinker = "zrtpqsdfghjklmwxcvbn";
        char temp;
        for (int i = 0; i < words; i++){
            int length = new Random().nextInt(maxLength) + 1;
            for (int j = 0; j < length; j++){
                int keuze = new Random().nextInt(3)+1;
                if (keuze == 3){
                    temp = klinker.charAt(new Random().nextInt(klinker.length()));
                }
                else {
                    temp = medeklinker.charAt(new Random().nextInt(medeklinker.length()));
                }

                if (j == 0 && camelCase){
                sb.append(Character.toUpperCase(temp));
                }
                else{
                    sb.append(temp);
                }
            }
            sb.append(" ");

        }

        return sb.toString();
    }

    private static LocalDate generateDate(){
        long min = LocalDate.of(1980, 1 , 1).toEpochDay();
        long max = LocalDate.now().toEpochDay();

        long rand = ThreadLocalRandom.current().nextLong(min, max);

        return LocalDate.ofEpochDay(rand);


    }

}
