package be.kdg.sneaker.kollections.sets;

import be.kdg.sneaker.kollections.Collection;
import be.kdg.sneaker.kollections.lists.List;

public interface Set<E> extends Collection<E> {
    List<E> toList();
}
