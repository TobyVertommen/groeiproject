package be.kdg.sneaker.kollections.maps;

import be.kdg.sneaker.kollections.Collection;
import be.kdg.sneaker.kollections.sets.Set;

public interface Map<K, V> {
    void put(K key, V value);
    V get(K key);
    Collection<V> values();
    Set<K> keySet();
    int size();
}
