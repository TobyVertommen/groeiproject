import be.kdg.sneaker.kollections.Kollections;
import be.kdg.sneaker.model.Model;
import be.kdg.sneaker.model.Sneaker;
import be.kdg.sneaker.model.SneakerFactory;

import java.time.LocalDate;
import java.util.Comparator;

import be.kdg.sneaker.kollections.lists.List;

import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Demo_8 {
    public static void main(String[] args) {
        Sneaker empty = SneakerFactory.newEmptySneaker();
        Sneaker filled = SneakerFactory.newFilledSneaker("Tobes Sneaker", 750, 43, true, Model.HIGH, LocalDate.now());

        System.out.println("Empty sneaker:" + empty);
        System.out.println("\nFilled sneaker:" + filled);


        System.out.println("30 Random sneakers sorted on name:");
        Stream.generate(SneakerFactory::newRandomSneaker).limit(30).sorted(Comparator.comparing(Sneaker::getName)).forEach(System.out::println);


        List<Sneaker> list = PerformanceTester.randomList(10);
        System.out.println("\nPerformance tester random:");
        Stream.of(list).forEach(s -> IntStream.iterate(0, i -> i + 1).limit(list.size()).forEach(i -> System.out.println(s.get(i))));

        //Compare
        PerformanceTester.compareArrayListAndLinkedList(10000); //20000 niet mogelijk


        List<Sneaker> listSelect = PerformanceTester.randomList(30);
        List<Sneaker> listMerge = listSelect;


        //Selection sort
        Kollections.selectionSort(listSelect);
        Stream.of(listSelect).forEach(s -> IntStream.iterate(0, i -> i + 1).limit(listSelect.size()).forEach(i -> System.out.println(s.get(i))));


        //Merge sort
        Kollections.mergeSort(listMerge);
        Stream.of(listMerge).forEach(s -> IntStream.iterate(0, i -> i + 1).limit(listMerge.size()).forEach(i -> System.out.println(s.get(i))));

        //Testers
        PerformanceTester.testSelectionSort();
        PerformanceTester.testMergeSort();


        //Quicksort
        System.out.println("\nQUICKSORT:");
        List<Sneaker> listQuick = PerformanceTester.randomList(30);
        Kollections.quickSort(listQuick);
        Stream.of(listQuick).forEach(s -> IntStream.iterate(0, i -> i + 1).limit(listQuick.size()).forEach(i -> System.out.println(s.get(i))));


        //Binary and linear search
        List<Sneaker> listBinary = PerformanceTester.randomList(30);
        Kollections.quickSort(listBinary);
        System.out.printf("Index of sneaker %s : %d\n", listBinary.get(18).getName(), Kollections.binarySearch(listBinary, listBinary.get(18)));
        System.out.printf("Index of sneaker %s : %d\n", listBinary.get(18).getName(), Kollections.lineairSearch(listBinary, listBinary.get(18)));

        Sneaker notPresent = SneakerFactory.newRandomSneaker();

        System.out.printf("Index of sneaker %s : %d\n", notPresent.getName(), Kollections.binarySearch(listBinary, notPresent));
        System.out.printf("Index of sneaker %s : %d\n", notPresent.getName(), Kollections.lineairSearch(listBinary, notPresent));


        PerformanceTester.compareListMapToHashMap(1000);


        System.out.println("\n");
        PerformanceTester.compareArraySetToTreeSet(1000);
    }
}
