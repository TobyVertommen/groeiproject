package be.kdg.sneaker.persist;

import be.kdg.sneaker.model.Sneaker;
import be.kdg.sneaker.model.Sneakers;

import java.io.*;

public class SneakerSerializer {
    public final String FILENAME;


    public SneakerSerializer(String name) {
        this.FILENAME = name;
    }

    public void serialize(Sneakers sneakers) throws IOException {
        FileOutputStream fos = new FileOutputStream(FILENAME);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(sneakers);
    }

    public Sneakers deserialize() throws IOException {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(FILENAME))){
            return (Sneakers) ois.readObject();
        }
        catch (IOException | ClassNotFoundException e){
            System.out.println("Deserialization failed!");
            return null;
        }
    }


}
