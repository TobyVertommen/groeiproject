package be.kdg.sneaker.persist;

import be.kdg.sneaker.model.Model;
import be.kdg.sneaker.model.Sneaker;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class SneakerDbDao implements SneakerDao {
    private Connection connection;
    private static final Logger logger = Logger.getLogger("be.kdg.persist.SneakerDbDao");

    public SneakerDbDao(String databasePath) {

        try {
            connection = DriverManager.getConnection("jdbc:hsqldb:file:" + databasePath, "sa", "");
        } catch (SQLException e) {
            logger.severe("Can't connect to: " + databasePath + "\nSql error: " + e.getMessage());
        }
        createTable();
    }


    public void close() {
        if (connection == null) return;
        try {
            Statement statement = connection.createStatement();
            statement.execute("SHUTDOWN COMPACT");
            statement.close();
            connection.close();
            System.out.println("\nDatabase closed");
        } catch (SQLException e) {
            logger.severe("Can't close the database\nSql error: " + e.getMessage());
            System.exit(1);
        }
    }

    private void createTable() {
        try {
            Statement statement = connection.createStatement();
            statement.execute("DROP TABLE sneakerstable IF EXISTS ");
            String createQuery = "CREATE TABLE sneakerstable " +
                    "(id INTEGER NOT NULL IDENTITY," +
                    "name VARCHAR(55) NOT NULL, " +
                    "price FLOAT NOT NULL," +
                    "size INTEGER NOT NULL," +
                    "profitable VARCHAR(25) NOT NULL," +
                    "model VARCHAR(25) NOT NULL," +
                    "releaseDate DATE)";
            statement.execute(createQuery);
            System.out.println("Database aangemaakt");
        } catch (SQLException e) {
            logger.severe("Error creating table: \nSQL error:" + e.getMessage());
            System.exit(1);
        }
    }

    @Override
    public boolean insert(Sneaker sneaker) {
        if (sneaker.getId() >= 0) return false;
        try {
            String query = "INSERT INTO sneakerstable VALUES (NULL, ?, ?, ?, ?, ?, ?)";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, sneaker.getName());
            preparedStatement.setFloat(2, (float) sneaker.getPrice());
            preparedStatement.setInt(3, sneaker.getSize());
            preparedStatement.setBoolean(4, sneaker.isProfitable());
            preparedStatement.setString(5, sneaker.getModel().name());
            preparedStatement.setDate(6, Date.valueOf(sneaker.getReleaseDate()));

            int rowsAffected = preparedStatement.executeUpdate();
            boolean result = rowsAffected == 1;
            preparedStatement.close();
            return result;

        } catch (SQLException e) {
            logger.severe("Error adding "+sneaker.getName()+" to table: \nSQL error:" + e.getMessage());

            return false;
        }
    }

    @Override
    public boolean delete(String name) {
        Statement statement = null;
        int rowsAffected = 0;
        if (name.equals("*")) {
            try {
                statement = connection.createStatement();
                rowsAffected = statement.executeUpdate("DELETE FROM sneakerstable");
                statement.close();
            } catch (SQLException e) {
                logger.severe("Error deleting "+name+" from table: \nSQL error:" + e.getMessage());

            }
        } else {
            try {
                String query = "DELETE FROM sneakerstable WHERE name = '" + name + "'";
                statement = connection.createStatement();
                rowsAffected = statement.executeUpdate(query);
            } catch (SQLException e) {
                logger.severe("Error deleting "+name+" from table: \nSQL error:" + e.getMessage());

            }
        }


        return rowsAffected == 1;
    }

    @Override
    public boolean update(Sneaker sneaker) {
        int rowsAffected = 0;
        try {
            String query = "UPDATE sneakerstable SET " +
                    "name = '" + sneaker.getName() + "', " +
                    "price = '" + sneaker.getPrice() + "', " +
                    "size = '" + sneaker.getSize() + "', " +
                    "profitable = '" + sneaker.isProfitable() + "', " +
                    "model = '" + sneaker.getModel().name() + "', " +
                    "releaseDate = '" + Date.valueOf(sneaker.getReleaseDate()) + "' " +
                    "WHERE id = '" + sneaker.getId()+"'";

            PreparedStatement preparedStatement = connection.prepareStatement(query);
            rowsAffected = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.severe("Error updating "+sneaker.getName()+" in the the table: \nSQL error:" + e.getMessage());
        }

        return rowsAffected == 1;
    }

    @Override
    public Sneaker retrieve(String name) {

        Sneaker sneaker = null;
        try {
            String query = "SELECT * FROM sneakerstable WHERE name IN('" + name + "')";
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                sneaker = new Sneaker(
                        rs.getString("name"),
                        rs.getDouble("price"),
                        rs.getInt("size"),
                        rs.getBoolean("profitable"),
                        Model.valueOf(rs.getString("model")),
                        rs.getDate("releaseDate").toLocalDate(),
                        rs.getInt("id"));


            }
        } catch (SQLException e) {
            logger.severe("Error retrieving"+name+" from table: \nSQL error:" + e.getMessage());

        }

        return sneaker;
    }

    @Override
    public List<Sneaker> sortedOn(String query) {
        List<Sneaker> result = new ArrayList<>();
        Sneaker temp;
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(query);
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                temp = new Sneaker(
                        rs.getString("name"),
                        rs.getDouble("price"),
                        rs.getInt("size"),
                        rs.getBoolean("profitable"),
                        Model.valueOf(rs.getString("model")),
                        rs.getDate("releaseDate").toLocalDate(),
                        rs.getInt("id"));

                result.add(temp);
            }


        } catch (SQLException e) {
            logger.severe("Error sorting the table: \nSQL error:" + e.getMessage());

        }
        return result;
    }


    public List<Sneaker> sortedOnPrice() {
        return sortedOn("SELECT * FROM sneakerstable ORDER BY price");
    }

    public List<Sneaker> sortedOnSize() {
        return sortedOn("SELECT * FROM sneakerstable ORDER BY size");
    }
}
