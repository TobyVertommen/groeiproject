package be.kdg.sneaker.persist;

import be.kdg.sneaker.model.Sneaker;

import java.util.List;

public interface SneakerDao {
    boolean insert(Sneaker sneaker);
    boolean delete(String name);
    boolean update(Sneaker sneaker);
    Sneaker retrieve(String name);
    List<Sneaker> sortedOn(String query);
}
