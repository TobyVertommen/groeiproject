package be.kdg.sneaker.persist;


import be.kdg.sneaker.data.Data;
import be.kdg.sneaker.model.Sneaker;
import be.kdg.sneaker.model.Sneakers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import java.util.List;


import static org.junit.jupiter.api.Assertions.*;

class SneakerSerializerTest {
    SneakerSerializer sneakerSerializer;
    Sneakers sneakers;




    @BeforeEach
    void setUp() {
        sneakers = new Sneakers();
        List<Sneaker> list = Data.getData();
        list.forEach(sneaker -> sneakers.add(sneaker));
        sneakerSerializer = new SneakerSerializer("db/sneakers.ser");

    }


    @Test
    void testSerialize() {
        try {
            sneakerSerializer.serialize(sneakers);
        } catch (IOException e) {
            fail("Serialization failed because exception was thrown!");
        }
    }


    @Test
    void testDeserialize() {
        try {
            assertEquals(sneakers.sneakerSet, sneakerSerializer.deserialize().sneakerSet);
        } catch (IOException e) {
            fail("Deserialization failed because exception was thrown!");
        }
    }
}