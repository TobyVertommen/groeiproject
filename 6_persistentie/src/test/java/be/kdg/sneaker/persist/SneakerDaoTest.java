package be.kdg.sneaker.persist;

import be.kdg.sneaker.data.Data;
import be.kdg.sneaker.model.Sneaker;
import org.junit.jupiter.api.*;

import java.util.Comparator;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class SneakerDaoTest {
        private SneakerDbDao dao;


        @BeforeAll
        public void start() {
            dao = new SneakerDbDao("db/sneakersDatabase"); //error?
        }

        @AfterAll
        public void stop(){
            dao.close();
        }

        @BeforeEach
        public void setUp(){
            Data.getData().forEach(sneaker -> dao.insert(sneaker));
        }

        @AfterEach
        public void last(){
            dao.delete("*");
        }

        @Test
        public void testInsert() {
            assertEquals(Data.getData().size(), dao.sortedOnSize().size());
        }

        @Test
        public void testRetrieveUpdate(){
            Sneaker first = dao.retrieve("Dunk Paisley");
            first.setName("Dunk Paisley Black");
            dao.update(first);
            assertEquals(first, dao.retrieve("Dunk Paisley Black"));
        }

        @Test
    public void testDelete() {
            int countBefore = dao.sortedOnSize().size();
            dao.delete("Vans SK8 mi");
            assertEquals(countBefore-1, dao.sortedOnSize().size());
            assertFalse(dao.delete("Vans SK8 mi"), "Delete failed");
        }

        @Test
        public void testSort(){
            assertArrayEquals(dao.sortedOnPrice().toArray(), Data.getData().stream().sorted(Comparator.comparing(Sneaker::getPrice)).toArray());
        }
    }


