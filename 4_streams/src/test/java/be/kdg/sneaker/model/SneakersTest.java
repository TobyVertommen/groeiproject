package be.kdg.sneaker.model;

import be.kdg.sneaker.data.Data;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class SneakersTest {
    private Sneaker s1;
    private Sneaker s2;
    private Sneaker s3;
    private Sneaker s4;

    private Sneakers ss;
    private Sneakers ss2;

    @BeforeEach
    private void setUp() {
        s1 = Data.getData().get(0);
        s2 = Data.getData().get(1);
        s3 = Data.getData().get(2);
        s4 = Data.getData().get(3);

        ss = new Sneakers();
        ss2 = new Sneakers(Data.getData());

        ss.add(s1);
        ss.add(s2);
        ss.add(s3);
        ss.add(s4);
    }

    @Test
    public void testAddToSneakerSet(){
        Sneaker s5 = Data.getData().get(4);
        assertTrue(ss.add(s5), "Sneaker may not be in the list already (duplicate)");

    }

    @Test
    public void testEmptySneakerSet(){
       // Sneaker s6 = Data.getData().get(5);
        assertTrue(ss.sneakerSet.remove(s4), "Sneaker is not in the list (missing)");

    }

    @Test
    public void testSortedOnName(){

        Sneaker s5 = Data.getData().get(4);
        assertAll(
                () -> assertEquals(ss2.sortedBy(Sneaker::getName).get(2), s1),
                () -> assertEquals(ss2.sortedBy(Sneaker::getName).get(4), s2),
                () -> assertEquals(ss2.sortedBy(Sneaker::getName).get(10), s5)
        );
    }

    @Test
    public void testSortedOnSize(){
        Sneakers ss3 = new Sneakers(Data.getData());
        assertArrayEquals(
                ss3.sortedBy(Sneaker::getSize).toArray(),
                ss2.sortedOnSize().toArray()
        );

    }
}