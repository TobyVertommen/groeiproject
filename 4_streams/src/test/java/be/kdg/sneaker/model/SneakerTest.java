package be.kdg.sneaker.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;


import static org.junit.jupiter.api.Assertions.*;

class SneakerTest {
    private Sneaker s1;
    private Sneaker s2;

    @BeforeEach
    private void setUp() throws Exception {
        s1 = new Sneaker("Air Jordan 1 Mocha", 350, 43, true, Model.HIGH, LocalDate.now());
        s2 = new Sneaker("Air Jordan 4 Metallic Red", 250, 43, false, Model.MID, LocalDate.parse("2001-01-01"));
    }

    @Test
    public void testEquals() {
        //Test fail
//         assertEquals(s1, s2, "These 2 objects are the same!");

        //Test success
        Sneaker s1copy = new Sneaker("Air Jordan 1 Mocha", 450, 44, false, Model.MID, LocalDate.parse("2001-03-21"));
        assertEquals(s1, s1copy, "These 2 objects are the same!");

    }

    @Test
    public void testIllegalName() {
        Sneaker sIll = new Sneaker("Airrrrr too", 450, 44, false, Model.MID, LocalDate.parse("2001-03-21"));
        //Test fail
//         assertThrows(IllegalArgumentException.class, () -> sIll.setName("Tobyyy"));

        //Test success
        assertThrows(IllegalArgumentException.class, () -> sIll.setName(null));
    }

    @Test
    public void testLegalName() {
        //Test fail
        //  assertDoesNotThrow(() -> s1.setName(null), "This name is not suitable!");


        //Test success
        assertDoesNotThrow(() -> s1.setName("Paisley dunk"), "This name is not suitable!");
    }

    @Test
    public void testCompareTo() {
        //Test fail
//        assertTrue( s1.compareTo(s2) > 0, "Sneaker should be sorted different in the list");

        //Test success
        Sneaker s1copy = new Sneaker("Air Jordan 1 Mocha", 350, 43, true, Model.HIGH, LocalDate.now());
        assertEquals(0, s1.compareTo(s1copy), "Sneaker should be sorted in same position in the list");

    }

    @Test
    public void testPrice() {
        assertEquals(350.0, s1.getPrice(), 0.002, "Price should be 50 and can differ 0.002 ");
    }
}