package util;


import java.util.List;
import java.util.function.Predicate;
import java.util.function.ToDoubleFunction;
import java.util.stream.Collectors;

public class SneakerFunctions {

    public static <T> List<T> filteredList(List<T> sneakers, Predicate<T> predicate){
     return sneakers.stream().filter(predicate).collect(Collectors.toList());
    }

    public static <T> Double average (List<T> sneakers, ToDoubleFunction<T> doubleMapper){
        return sneakers.stream().mapToDouble(doubleMapper).average().getAsDouble();
    }


    public static <T> long countIf(List<T> sneakers, Predicate<T> predicate){
        return sneakers.stream().filter(predicate).count();
    }

}
