
import be.kdg.sneaker.data.Data;
import be.kdg.sneaker.model.Model;
import be.kdg.sneaker.model.Sneaker;
import be.kdg.sneaker.model.Sneakers;
import util.SneakerFunctions;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

public class Demo_4 {

    public static void main(String[] args) {

        //2.2
        Sneakers list = new Sneakers();
        Data.getData().forEach(list::add);

        System.out.println("Op naam gesorteerd:");
        list.sortedBy(Sneaker::getName).forEach(System.out::println);

        //2.4
        List<Sneaker> sneakerList = new ArrayList<>(Data.getData());
        sneakerList = SneakerFunctions.filteredList(sneakerList, sneaker -> sneaker.getSize() > 40);
        System.out.println(sneakerList);


        //2.6
        sneakerList = new ArrayList<>(Data.getData());
        System.out.printf("Average Sneaker price: %.1f euro\n", SneakerFunctions.average(sneakerList, Sneaker::getPrice));

        //2.8
        sneakerList = new ArrayList<>(Data.getData());
        System.out.printf("Sum of Sneakers with price > 400 euro: %d\n", SneakerFunctions.countIf(sneakerList, sneaker -> sneaker.getPrice() > 400));


        //3.1
        sneakerList = new ArrayList<>(Data.getData());
        System.out.printf("Sum of Sneakers released before 2004: %d", sneakerList.stream().filter(sneaker -> sneaker.getReleaseDate().isBefore(LocalDate.parse("2004-01-01"))).count());

        //3.2
        System.out.println("All Sneakers sorted on price and after that on size");
        Collections.sort(sneakerList, Comparator.comparing(Sneaker::getPrice).thenComparing(Sneaker::getSize));
        sneakerList.forEach(System.out::println);


        //3.3
        sneakerList = new ArrayList<>(Data.getData());
        System.out.println("\nAll Sneaker names in CASE, sorted backwards and without duplicates");
        sneakerList.stream().map(Sneaker::getName).distinct().sorted(Comparator.reverseOrder()).forEach(sn -> System.out.printf("%s \n", sn));


        //3.4
        var below = sneakerList.stream().filter(sneaker -> sneaker.getSize() <42).findAny();
        System.out.printf("\nRandom Sneaker with shoe size below EU42: %s %dEU\n", below.get().getName(), below.get().getSize() );

        //3.5
        System.out.printf("\nChampion in highest price: %s\n", sneakerList.stream().max(Comparator.comparing(Sneaker::getPrice)).get().getName());

        System.out.println("Champion in latest release: "+ sneakerList.stream().max(Comparator.comparing(Sneaker::getReleaseDate)).get().getName()+"\n");


        //3.6
        System.out.println("List with sorted sneaker names starting with 'A'\n"
         + Arrays.toString(sneakerList.stream()
                .map(Sneaker::getName)
                .filter(name -> name.startsWith("A"))
                .toArray())
        );


        //3.7
        Map<Boolean, List<Sneaker>> map = sneakerList.stream().sorted(Comparator.comparing(Sneaker::getReleaseDate)).collect(Collectors.partitioningBy(sn -> sn.getReleaseDate().isBefore(LocalDate.parse("2005-01-01"))));

        System.out.println("Sublist with sneakers released before 2005:");
        map.get(true).forEach(System.out::println);

        System.out.println("Sublist with sneakers released after 2005:");
        map.get(false).forEach(System.out::println);



        //3.8
        System.out.println("All sneakers grouped by model:");
        Map<Model, List<Sneaker>> map2 = sneakerList.stream().sorted(Comparator.comparing(Sneaker::getName)).collect(Collectors.groupingBy(Sneaker::getModel));
        map2.forEach((k, v) -> System.out.printf("%-8s : %10s\n",
                k, v.stream()
                        .map(Sneaker::getName).collect(Collectors.joining(", "))));
        System.out.println("Build succesful!");


    }
}